#include "ptztracing.h"
#include "mymath.h"
#include "ctrlaw.h"

extern uint8_t b_FlagInitCtrlPTZVal;    //云台控制量初始化标志
float tilt_ctrl=0, roll_ctrl = 0;       //云台tilt,roll的控制量
float tilt_E0=0, roll_E0=0;    
int16_t TrimTILT = 1500, TrimROLL = 1500;    //遥控器中值,tilt=1200对应-90度，相机下视
int16_t PWMTILT = 1500, PWMROLL = 1500;      //遥控器输出,


void PTZ_Ctrl(int16_t tarPosX, int16_t tarPosY, uint8_t VisionLocking_flag)
{
	
		if(b_FlagInitCtrlPTZVal == 1)
		{
			b_FlagInitCtrlPTZVal = 0;
			Ctrl_Init(); 
		}
		
		//if(VisionLocking_flag == 1)  //视觉锁定后，控制云台运动跟踪目标，否则视为云台已经跟踪上目标，像素偏差为0，云台不动
		//{
			tilt_E0 = tarPosX;    //假设相机高度为2米时，解算出的
			roll_E0 = -tarPosY;     //TODO:需要对接收到的数据做限幅等正确性检查
		//}
		//else
		//{
		//	tilt_E0 = 0;
		//	roll_E0 = 0;
		//}
		
//		if(VisionLocking_flag == 1)
//		{
				tilt_ctrl = PositionPID(tilt_E0, &pid_tilt);
				roll_ctrl = PositionPID(roll_E0, &pid_roll);
//		}
//		else
//		{
//			//Ctrl_Init();
//			tilt_ctrl = 0;
//			roll_ctrl = 0;
//		}
		
		
}

void PWM_Cal(void)
{
	//tilt,roll控制量转换为PPM信号
	PWMTILT = tilt_ctrl*1 + TrimTILT;
	PWMROLL = roll_ctrl*1 + TrimROLL;
}
