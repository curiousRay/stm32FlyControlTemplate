/*
  ******************************************************************************************************
  * @file     
  * @author  XY
  * @brief   Control Law PID
  
             (1) Ordinary Double Loop
			 (2) Double Rate
  ******************************************************************************************************
*/

#ifndef _CTRLAW_H_
#define _CTRLAW_H_
#include "stm32f4xx.h"

typedef struct PID  
{
    float P;     //Proportional Const
    float I;     //Integral Const
    float D;     //Derivative Const
    float BE;    //k-2 Error
    float LE;    //k-1 Error
    float PE;    //k Error
    float CTRL;
}PID;

extern struct PID pid_roll, pid_tilt;

void Ctrl_Init(void);
void PID_Init(void);
float IncrementPID(float, PID *);
float PositionPID(float, PID *);

#endif
