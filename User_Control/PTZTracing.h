/**
  ******************************************************************************
  * @file    ptztracing.h
  * @author  zbl
  * @version V1.0.0
  * @date    17-7-2016
  * @brief   This file contains all the functions prototypes for the PTZ-tracing.
  ******************************************************************************
  * @attention
  *
  * 
  *
  * <h2><center>&copy; COPYRIGHT 2016 zbl</center></h2>
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PTZTRACING_H
#define __PTZTRACING_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"



/* Exported types ------------------------------------------------------------*/
extern float tilt_ctrl, roll_ctrl;       //云台tilt,roll的控制量
extern int16_t PWMTILT, PWMROLL;      //遥控器输出
/** 
  * @brief  communication Init Structure definition  
  */
	 

	 
/* Exported constants --------------------------------------------------------*/

	 
/**
  * @}
  */

/**
  * @}
  */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
void PTZ_Ctrl(int16_t tarPosX, int16_t tarPosY, uint8_t VisionLocking_flag);
void PWM_Cal(void);

#ifdef __cplusplus
}
#endif

#endif /* __PTZTRACING_H */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2016/7/17 zbl *****END OF FILE****/
