/*
  ******************************************************************************************************
  * @file     
  * @author  XY
  * @brief   Mathematics
			 
  ******************************************************************************************************
*/

#ifndef _MYMATH_H_
#define _MYMATH_H_
#include "stm32f4xx.h"

#include "math.h"
#include "arm_math.h"

#define d2r 0.01745
#define pi 3.1412

#define ROLL  	1
#define PITCH 	2
#define YAW   	3
#define AXIS_X  1
#define AXIS_Y	2
#define AXIS_Z  3

float Min(float, float);
float Max(float, float);
float AMP_Limit(float, float, float);

int Get_Comma(u16, u8[]);
int Get_Star(u16, u8[]);

float ASCII2FLOAT(s16, s16, u8[]);
double ASCII2DOUBLE(s16, s16, u8[]);
u8 FLOAT2ASCII(float f, u8[]);

s8 INT2ASCII(s32, u8[]);
s16 ASCII2INT(s16, s16, u8[]);

float UCHAR2FLOAT(u8[], s16);
void FLOAT2UCHAR(float, s16, u8[]);

float BYTES2FLOAT(u8[], s16);
void FLOAT2BYTES(float, s16, u8[]);

#endif
