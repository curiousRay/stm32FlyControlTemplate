/*
  ******************************************************************************************************
  * @file     
  * @author  XY
  * @brief   Control Law PID
  
             (1) Ordinary Double Loop
			 (2) Double Rate
  ******************************************************************************************************
*/

#include "mymath.h"
#include "ctrlaw.h"

struct PID pid_roll, pid_tilt;
float sum_E0;

void Ctrl_Init(void)
{
                               //e(k-2)            //e(k-1)           //e(k)
    pid_roll.CTRL = 0;    pid_roll.BE = 0;    pid_roll.LE = 0;    pid_roll.PE = 0;           
    pid_tilt.CTRL = 0;    pid_tilt.BE = 0;    pid_tilt.LE = 0;    pid_tilt.PE = 0;    
         
}

void PID_Init(void)
{
		Ctrl_Init();
	      
    pid_roll.P = 0.7;    
    pid_roll.I = 0;        
    pid_roll.D = 0;
      
    pid_tilt.P = 0.7; //0.5  
    pid_tilt.I = 0;         
    pid_tilt.D = 0;   
     
}


/* Increment PID */ 
float IncrementPID(float E0, PID *pid_s)
{    
	float E1, E2, CO;   
	float P, I, D;
	                       
	//E0 = SP - PV;                        //e(k)
	E1 = pid_s->LE;                      //e(k-1)
	E2 = pid_s->PE;                      //e(k-2)
	
	P = pid_s->P * (E0-E1);           //P
	I = pid_s->I * E0;                //I
	D = pid_s->D * (E0-2*E1+E2);      //D
	
// 	P = AMP_Limit(P, -25, 25);
 // 	I = AMP_Limit(I, -2, 2);
// 	D = AMP_Limit(D, -2, 2);
	
	CO = P + I + D + pid_s->CTRL;
	
	pid_s->PE = E1;                      //Save erro
	pid_s->LE = E0;   
	pid_s->CTRL = CO;                     
	return(CO); 
}


/* Position PID */ 
float PositionPID(float E0, PID *pid_s)
{    
	float E1,CO;   
	float P, I, D;
	                       
	//E0 = SP - PV;                        //e(k)
	E1 = pid_s->LE;                      //e(k-1)
	//E2 = pid_s->PE;                      //e(k-2)
	sum_E0 = sum_E0 + E0;
	
	P = pid_s->P * E0;           //P
	I = pid_s->I * sum_E0;       //I
	D = pid_s->D * (E0-E1);      //D
	
// 	P = AMP_Limit(P, -25, 25);
 // 	I = AMP_Limit(I, -2, 2);
// 	D = AMP_Limit(D, -2, 2);
	
	CO = P + I + D;
	
	pid_s->PE = E1;                      //Save erro
	pid_s->LE = E0;   
	pid_s->CTRL = CO;                     
	return(CO); 
}

