/*
  ******************************************************************************************************
  * @file     
  * @author  XY
  * @brief   Mathematics
			 
  ******************************************************************************************************
*/

#include "mymath.h"

/*******************************************************************************************************
                                         Limit Amplitude 
********************************************************************************************************/
float Min(float in, float low)
{
     if (in < low)
     	in = low;
     
     return in;
}

float Max(float in, float up)
{
	if (in > up)
     	in = up;
     
     return in;
}

float AMP_Limit(float in, float low, float up)
{
     if (in < low)
     	in = low;
     else if (in > up)
     	in = up;
     
     return in;
}

/*******************************************************************************************************
                                            Find Mark
********************************************************************************************************/
/* ','+1 */
int Get_Comma(u16 num, u8 str[])
{
	s16 i = 0, j = 0;
	
	while (i < 200)
	{				
		if (str[i] == ',')
			j++;
		if (j == num)
			return i+1;		
			
		i++;	
	}

	return 0;	
}

/* '*'+1 */
int Get_Star(u16 num, u8 str[])
{
	s16 i = 0, j = 0;
	
	while (i < 200)
	{				
		if (str[i] == '*')
			j++;
		if (j == num)
			return i+1;		
			
		i++;	
	}

	return 0;	
}

/*******************************************************************************************************
                                       Algorithm for Matrix
********************************************************************************************************/
void MatrixAdd(float Amat[][3], float Bmat[][3], float Cmat[][3])
{
	
}

void MatrixMultip(float Amat[][3], float Bmat[][3], float Cmat[][3])
{
	
}

/*******************************************************************************************************
                                       Float ---- 32bits Binary
********************************************************************************************************/
/* 32bits char to float */
float BINARY2FLOAT(s16 n, s8 str[])
{
	s16 i;
	s32 ch = 0;
	float re;

	// 逆位
	for (i=n; i<4+n; i++)      
        ch |= (str[i] & 0xFF) << ((i-3) * 8);
	re = *(float*) & ch;

	return re;
}

/* 32bits char to float */
void FLOAT2BINARY(float f, s8 str[])
{
	
}

/*******************************************************************************************************
                                           ASCII ---- Float  
********************************************************************************************************/
/* ASCII to float */
float ASCII2FLOAT(s16 n, s16 m, u8 str[])
{	
	s16 i;
	int point=0;                     //小数点标志
	int negative=0;                  //负数标志
	int decimal=0;                     //小数位
	float integer;                     //整数
	float rev=0;
	
	if (str[n]=='-')
	{
		negative = 1;
		n++;
	}
	
	for (i=n; i<m; i++)
	{
		if (str[i]=='.')
		{
			point = 1;
			decimal = 1;
			continue;			
		}		 
		else 
			integer = str[i]-0x30;
		
		// 处理小数部分
		if (point==1)
		{
			rev = rev + integer/(10 * decimal);
			decimal = decimal*10 ;
		}	
		// 处理整数部分
		else
			rev = rev*10+integer;
	}
	
	if (negative==1)
		return -1*rev;
	else
		return rev;
}

/* ASCII to float */
double ASCII2DOUBLE(s16 n, s16 m, u8 str[])
{	
	s16 i;
	int point=0;                     //小数点标志
	int negative=0;                  //负数标志
	int decimal=0;                     //小数位
	double integer;                     //整数
	double rev=0;
	
	if (str[n]=='-')
	{
		negative = 1;
		n++;
	}
	
	for (i=n; i<m; i++)
	{
		if (str[i]=='.')
		{
			point = 1;
			decimal = 1;
			continue;			
		}		 
		else 
			integer = str[i]-0x30;
		
		// 处理小数部分
		if (point==1)
		{
			rev = rev + integer/(10 * decimal);
			decimal = decimal*10 ;
		}	
		// 处理整数部分
		else
			rev = rev*10+integer;
	}
	
	if (negative==1)
		return -1*rev;
	else
		return rev;
}

u8 FLOAT2ASCII(float f, u8 ascii[])
{	
	u8 num = 0;
	u16 integ = 0;
	float decimal = 0.0;
	
	u8 i;
	u8 str[10];
	u8 n_integ = 0;
	
	if (f < 0)
	{
		ascii[num++] = '-';
		f = -1*f;
	}
	integ = (u16)f;
	decimal = f - (float)integ;
	
	/* 整数部分 */
	if (integ == 0)
	{
		ascii[num++] = 0x30;
	}
	else
	{
		while (integ > 0)
		{
			str[n_integ++] = integ%10 + 0x30;
			integ /= 10;			
		}
		for(i=0; i<n_integ; i++)
		{
			ascii[num++] =  str[n_integ-1-i];
		}
	}
	ascii[num++] = '.';
	
	/* 小数部分 */
	/* decimal是无限循环小数，仅保留2位有效数字 */
	for (i=0; i<2; i++)
	{
		integ = (u16)(decimal*10);
		ascii[num+i] =  integ + 0x30;
		decimal = decimal*10 - (float)integ;
	}	
	
	num += 2;
	return num;
}

/*******************************************************************************************************
                                           ASCII ---- Integer  
********************************************************************************************************/
/* Integer to ASCII */
s8 INT2ASCII(s32 integ, u8 ascii[])
{
	s8 i;
	s8 n = 0;                                // 位数
	
	if (integ < 10) 
		n = 1;
	else if (integ < 100) 
		n = 2;
	else if (integ < 1000) 
		n = 3;
	else if (integ < 10000) 
		n = 4;
	else if (integ < 100000) 
		n = 5;
	else if (integ < 1000000) 
		n = 6;
	
	for (i=n-1; i>=0; i--)
	{
		ascii[i] = integ%10 + 0x30;
		integ /= 10;
	}
	
	return n;
		
}

/* ASCII to Integer */
s16 ASCII2INT(s16 n, s16 m, u8 ascii[])
{
	s16 integ = 0;
	s8 i;

	for (i=n; i<m; i++)
	{
		integ = integ*10 + (ascii[i] - 0x30);
	}
	
	return integ;		
}

/*******************************************************************************************************
                                  Clover: 3 Bytes can represent one float
                                          LSB First
********************************************************************************************************/
/* 3 bytes to one float */
float UCHAR2FLOAT(u8 str[], s16 n)
{
	s16 i;
	u8 exr[3];	
	float res;
		
	for (i=0; i<3; i++)
		exr[i] = str[i+3+n];
		
	res = exr[1] + 0.01*exr[2];
	if (exr[0] == '-')
		return -1*res;
	else
		return res;
		
}	

/* one float to 3 bytes uchar*/
void FLOAT2UCHAR(float f, s16 n, u8 str[])
{	
	float f1;
	u16 c;
	
	/* <255 整数部分有效位2位 */
	if (f < 0)
	{
		str[0+n]='-';
		f=-1*f;
		
		f1=f;
		str[1+n]=(u8)f;
		c=(u16)(f1*100);
		c=c%100;
		str[2+n]=(u8)c;	
	}
	else if (f < 255)
	{
		str[0+n]='+';
	
		f1=f;
		str[1+n]=(u8)f;
		c=(u16)(f1*100);
		c=c%100;
		str[2+n]=(u8)c;	
	}
	
	/* >255 整数部分有效位3位 */
	else
	{
		f1=f;
		str[0+n]=(u16)f>>8;		
		str[1+n]=f1;
		c=(u16)(f1*100);
		c=c%100;
		str[2+n]=(u8)c;	
	}		

}

/*******************************************************************************************************
                                  General: 2 Bytes can represent one float 
                                           LSB First
********************************************************************************************************/
/* 2 bytes to one float */
float BYTES2FLOAT(u8 str[], s16 n)
{
	float data;
	u16 data1,data2;

	data1=str[n];
	data2=str[n+1];
	data=data1+(data2<<8);
	data=data-100;
	
	//data = str[n] + (s16)(str[n+1]<<8);
	return data*0.01f;
}	

/* one float to 2 bytes uchar*/
void FLOAT2BYTES(float f, s16 n, u8 str[])
{	
	u16 data;
	//f=f*100+100;
	//data = (u16)f;
	data = f*1000+1000;
    str[n] = data;	
	str[n+1] = data>>8;
}



