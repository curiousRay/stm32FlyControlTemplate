#include "stm32f4xx.h"
#include "communication.h"

extern struct UART_TypeDefStruct pUART2;         //uart数据解包及接收缓冲相关结构体
//uint8_t RxArr[1000]={0};

void USART2_Init(void)
{	
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef  USART_InitStructure;
	USART_ClockInitTypeDef    USART_ClockInitStruct;
	NVIC_InitTypeDef NVIC_InitStructure;
    
	/*USART2 clock enable.  */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	/* GPIOA clock enable */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/* USART2 Interrupt enable */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Connect USART2 pin to AF7 */
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_USART2);
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource6, GPIO_AF_USART2);
	
	/* USART2_Tx configuration : PD.05 */
	GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
	/* USART2_Rx configuration : PD.6 */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	/* configuration USART2 */
	USART_StructInit(&USART_InitStructure);
	USART_InitStructure.USART_BaudRate = 115200 ;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx |USART_Mode_Tx ;
	USART_Init(USART2 , &USART_InitStructure);
	USART_ClockStructInit(&USART_ClockInitStruct);
	USART_ClockInit(USART2,&USART_ClockInitStruct);
		
	/* Interrupts and flags management functions **********************************/
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	/* Enable USART2 */
	USART_Cmd(USART2, ENABLE);

}

/* receive data interrupt of USART2 */
void USART2_ISR(void)
{
	  uint8_t RxByte;
		//static uint32_t i=0;
	
	  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)//
	  {		
//			
		  

		/* Read one byte from the receive data register */
		RxByte = USART_ReceiveData(USART2);	
//		RxArr[i++] = RxByte;
//		if(i>= 1000)
//			i=0;
		UARTGetByte(USART2, RxByte, &pUART2);
	  
		
	  }		
	//	USART_ClearFlag(USART2,USART_FLAG_RXNE);
	//	USART_ClearITPendingBit(USART2, USART_IT_RXNE);
//	  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)//
//	  {
//		/*clear the interrupt flag*/
//		USART_ClearITPendingBit(USART2, USART_IT_RXNE);  
//	  }
 
}



/*Test_Usart2该程序用来检测Usart2

void Test_Usart2()
{
 		printf("\rHello World!");
 		Delay(1000);
		

}
*/
