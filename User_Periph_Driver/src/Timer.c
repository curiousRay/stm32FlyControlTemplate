#include "Timer.h"
#include "LED.h"
#include "stm32f4xx.h"
#include "communication.h"

uint16_t capture = 0;
extern uint16_t nus;
extern uint16_t acount;
extern u8 time_100ms_flag;
extern uint32_t IC2Value;
extern uint32_t PWM_Input_Freq;
extern uint32_t m;

extern union VisionFlyData_USART visionflydata_usart;   //视觉与飞控的通信联合体

void TIM3_PWMOUT_Init(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	uint32_t PrescalerValue_PWM;
	
	/* TIM2 clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
   /* GPIOB clock enable */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);  
	/* GPIOB Configuration: TIM2 CH3 (PB0) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOB, &GPIO_InitStructure);   
	/* Connect TIM2 pins to AF1 */  
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource0, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource1, GPIO_AF_TIM3);
   
	/* Compute the TIM2_prescaler value */
	PrescalerValue_PWM = (uint32_t)(((SystemCoreClock/2)/1000000) - 1);//1MHz
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 20000-1;					
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue_PWM;  //T=2000/1000MHz=2ms  PWM有效范围[1000~2000]
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
  /* PWM1 Mode configuration: Channel_3 */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;//PWM有两种模式参看手册
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC3Init(TIM3, &TIM_OCInitStructure);     
	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_OC4Init(TIM3, &TIM_OCInitStructure);     
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM3, ENABLE);

	/* TIM2 enable counter */
	TIM_Cmd(TIM3, ENABLE);

}

void TIM3_PWMDuty(uint16_t widthCH3, uint16_t widthCH4)
{
	TIM3->CCR3 = widthCH3;  
	TIM3->CCR4 = widthCH4;    
}



void TIM2_Init(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
//	TIM_OCInitTypeDef  TIM_OCInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
    uint32_t PrescalerValue;
	
	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	/* Enable the TIM3 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Compute the prescaler value */
	PrescalerValue = (uint32_t) ((SystemCoreClock / 2) / 1000000) - 1; //1MHz
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 100000 - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	/* Prescaler configuration */
	TIM_PrescalerConfig(TIM2, PrescalerValue, TIM_PSCReloadMode_Immediate);

	/* Output Compare Timing Mode configuration: Channel1 */
//	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
//	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure.TIM_Pulse = nus;                    //计数到CCR1_Val中断 单位ms
//	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
//	TIM_OC1Init(TIM2, &TIM_OCInitStructure);
//	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Disable);

   
	/* TIM Interrupts enable */
	TIM_ITConfig(TIM2, TIM_IT_Update , ENABLE);

	/* TIM3 enable counter */
	TIM_Cmd(TIM2, ENABLE);
}
void TIM2_ISR(void)
{
	static uint8_t tim2_flag = 0x00;

	//SendMessageToVision(USART2, 0x06, visionflydata_usart.data_c);  //发送数据给视觉系统
		
	time_100ms_flag=1;

	
	if(tim2_flag)
	{
		LED_ON(2); 	 
	}
	else
	{
		LED_OFF(2); 
	}
	tim2_flag = ~tim2_flag;	
			
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	  {
			TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
			
    }

}


void TIM4_PWMIN_Init(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_ICInitTypeDef  TIM_ICInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	uint32_t PrescalerValue_PWM_INPUT;
	
	/* TIM4 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	/* GPIOB clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	/* TIM4 chennel2 configuration : PB.07 */
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;   //alter function 即复用功能开启
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP ;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	/* Connect TIM pin to AF2 */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_TIM4);
	
	/* Enable the TIM4 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Compute the TIM4_prescaler value */
	PrescalerValue_PWM_INPUT = (uint16_t)(((SystemCoreClock/2)/1000000) - 1);//1MHZ
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 65535;					
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue_PWM_INPUT;		
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	
	/* TIM4 configuration: PWM Input mode ------------------------
     The external signal is connected to TIM4 CH2 pin (PB.07), 
     The Rising edge is used as active edge,
     The TIM4 CCR2 is used to compute the frequency value 
     The TIM4 CCR1 is used to compute the duty cycle value
  ------------------------------------------------------------ */

	TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStructure.TIM_ICFilter = 0x0;
	TIM_PWMIConfig(TIM4, &TIM_ICInitStructure);
	/* Select the TIM4 Input Trigger: TI2FP2 */
	TIM_SelectInputTrigger(TIM4, TIM_TS_TI2FP2);
	/* Select the slave Mode: Reset Mode */
	TIM_SelectSlaveMode(TIM4, TIM_SlaveMode_Reset);
	TIM_SelectMasterSlaveMode(TIM4,TIM_MasterSlaveMode_Enable);
	/* TIM enable counter */
	TIM_Cmd(TIM4, ENABLE);
	/* Enable the CC2 Interrupt Request */
	TIM_ITConfig(TIM4, TIM_IT_CC2, ENABLE);
}
void TIM4_ISR(void)
{
 
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);

	/* Clear TIM4 Capture compare interrupt pending bit */
	TIM_ClearITPendingBit(TIM4, TIM_IT_CC2);
	/* Get the Input Capture value */
	IC2Value = TIM_GetCapture2(TIM4);
	if (IC2Value != 0)
	{
	   /* Frequency computation 
       TIM4 counter clock = 1000000Hz */
	   PWM_Input_Freq =(1000000 / IC2Value);

	}
	else
	{
		PWM_Input_Freq = 0;
	}
}

/*void Test_Time(void)--------测试定时器时使用----	
if(time_1s_flag)
		{
			time_1s_flag=0;
			acount=0;
			printf("\r\nfrequency:%10d\r\nIC2Value:%10d",PWM_Input_Freq,IC2Value);
			TIM2->ARR=Period;          //改变频率
			Period+=1000;
			TIM2_PWMDuty(PWMDuty);  //改变占空比->示波器观察
			PWMDuty=PWMDuty+100;
            if(PWMDuty=2000)
            PWMDuty=1000;
            if(Period==10000)
			Period=3000;
			
             
		}
******/


