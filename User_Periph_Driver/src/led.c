#include "LED.h"

void LED_Init(void)
{
	/*Configure LED*/
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
}
void LED_ON(uint8_t led)
{
	switch(led)
	{
		case 1: GPIO_SetBits(GPIOD,GPIO_Pin_12);break;
		case 2: GPIO_SetBits(GPIOD,GPIO_Pin_13);break;
		case 3: GPIO_SetBits(GPIOD,GPIO_Pin_14);break;
		case 4: GPIO_SetBits(GPIOD,GPIO_Pin_15);break;
		default:break;
	}
}

void LED_OFF(uint8_t led)
{
	switch(led)
	{
		case 1: GPIO_ResetBits(GPIOD,GPIO_Pin_12);break;
		case 2: GPIO_ResetBits(GPIOD,GPIO_Pin_13);break;
		case 3: GPIO_ResetBits(GPIOD,GPIO_Pin_14);break;
		case 4: GPIO_ResetBits(GPIOD,GPIO_Pin_15);break;
		default:break;
	}	
}





