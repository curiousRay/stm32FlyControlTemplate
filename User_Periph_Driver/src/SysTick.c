/*�δ�ʱ��ʱ��1ms*/
#include "SysTick.h"
static __IO uint32_t TimingDelay;
void SysTick_Init(void)
{
	if (SysTick_Config(SystemCoreClock / 1000))     // Reload Value = SysTick Counter Clock (Hz) x  Desired Time base (s)
  {
    /* Capture error */
    while (1);
  }
}

void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
}
void Delay(__IO uint32_t nTime)
{
  TimingDelay = nTime;

  while(TimingDelay != 0);   
}



/*@@*/
