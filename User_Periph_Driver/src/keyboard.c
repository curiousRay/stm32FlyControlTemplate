
/*-------------------------------------
*								4×4矩阵键盘驱动
*	文件：	keyboard.c keyboard.h
*   作者：  孔大庆
*   描述：  扫描4×4矩阵键盘并返回1-16编码值
*   说明:   包含两个函数
								void KeyBoard_Init(void)——初始化键盘输入输出口；
								u8  Read_KeyValue(void)——扫描键盘并返回编码值；
						引脚分配：使用PE0-PE3为输出——行扫描：红黄绿蓝（杜邦线颜色）
					            使用PE4-PE7为输入——列扫描：褐灰白黑
*   时间：  2014年12月1日
    修改：  2015/7/4
---------------------------------------*/
#include "keyboard.h"
#include "SysTick.h"

void KeyBoard_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	/*Enable GPIOD CLOCK*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE , ENABLE);    //GPIOE->GPIOD

	/* Configure PE0, PE1, PE2 and PE3 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1| GPIO_Pin_2| GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	/* Configure PE4, PE5, PE6 and PE7 in input pushpulldown mode */	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5| GPIO_Pin_6| GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;//下拉输入
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	/*Init GPIOD*/
	GPIO_SetBits(GPIOE, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3);
	GPIO_ResetBits(GPIOE, GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7);
}

u8 Read_KeyValue(void)
{
	u8 KeyValue=16;
	if((GPIO_ReadInputData(GPIOE)&0xff)!=0x0f) //判断输入口高四位有没有出现高电平
	{
		Delay(10);
		if((GPIO_ReadInputData(GPIOE)&0xff)!=0x0f)//再次判断
		{
			GPIO_SetBits(GPIOE, GPIO_Pin_0);
			GPIO_ResetBits(GPIOE, GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3);
			switch(GPIO_ReadInputData(GPIOE)&0xff)
			{
				case 0x11: KeyValue = 1; break;
				case 0x21: KeyValue = 4; break;
				case 0x41: KeyValue = 7; break;
				case 0x81: KeyValue = 15;break; // *
				
			}
			GPIO_SetBits(GPIOE, GPIO_Pin_1);
			GPIO_ResetBits(GPIOE, GPIO_Pin_0 | GPIO_Pin_2 | GPIO_Pin_3);
			switch(GPIO_ReadInputData(GPIOE)&0xff)
			{
				case 0x12: KeyValue = 2; break;
				case 0x22: KeyValue = 5; break;
				case 0x42: KeyValue = 8;break;
				case 0x82: KeyValue = 0;break;
				
			}
			GPIO_SetBits(GPIOE, GPIO_Pin_2);
			GPIO_ResetBits(GPIOE, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3);
			switch(GPIO_ReadInputData(GPIOE)&0xff)
			{
				case 0x14: KeyValue = 3; break;
				case 0x24: KeyValue = 6; break;
				case 0x44: KeyValue = 9;break;
				case 0x84: KeyValue = 14;break; //#
			
			}
			GPIO_SetBits(GPIOE, GPIO_Pin_3);
			GPIO_ResetBits(GPIOE, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2);
			switch(GPIO_ReadInputData(GPIOE)&0xff)
			{
				case 0x18: KeyValue = 10;break; //A
				case 0x28: KeyValue = 11;break; //B
				case 0x48: KeyValue = 12;break; //C
				case 0x88: KeyValue = 13;break; //D
				
			}
			GPIO_SetBits(GPIOE, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3);
			GPIO_ResetBits(GPIOE, GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7);
			while((GPIO_ReadInputData(GPIOE)&0xff)!=0x0f);
			
		}
	}
	return KeyValue;
		
}

/*这段程序用来检测键盘有无问题*/
/*
void Test_Key(void)
{
	switch(Read_KeyValue())
		{
			case 0:LED_ON(led1);break;
			case 1:LED_OFF(led1);break;
			case 2:LED_ON(led2);break;
			case 3:LED_OFF(led2);break;
			case 4:LED_ON(led3);break;
			case 5:LED_OFF(led3);break;
			case 6:LED_ON(led4);break;
			case 7:LED_OFF(led4);break;
			case 8:LED_ON(led1);LED_ON(led2);break;
			case 9:LED_OFF(led1);LED_OFF(led2);break;
			case 10:LED_ON(led2);LED_ON(led3);break;
			case 11:LED_OFF(led2);LED_OFF(led3);break;
			case 12:LED_ON(led3);LED_ON(led4);break;
			case 13:LED_OFF(led3);LED_OFF(led4);break;
			case 14:LED_ON(led1);LED_ON(led4);break;
			case 15:LED_OFF(led1);LED_OFF(led4);break;
			default:break;
			
		}
}
*/
