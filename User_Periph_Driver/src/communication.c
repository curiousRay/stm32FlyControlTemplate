#include "communication.h"


int8_t uart1_error = 0;  //串口通信错误标志
union VisionFlyData_USART visionflydata_usart;   //视觉与飞控的通信联合体
struct UART_TypeDefStruct pUART2;         //uart数据解包及接收缓冲相关结构体

extern int16_t tarPosX; //目标在X方向上的偏差，单位：mm
extern int16_t tarPosY; //目标在Y方向上的偏差，单位：mm
extern uint8_t VisionLocking_flag;  //视觉锁定目标的状态：0--目标丢失，1--正在锁定，2--已经锁定

/*communication relative functions */

//将收到的数据报保存到缓冲区,每来一个字节进一次中断(也可改为全部接受完，触发一次中断)
void UARTGetByte(USART_TypeDef* USARTx, uint8_t R_chardata, struct UART_TypeDefStruct * pUART)
{

	switch(pUART->RecvParse.R_state)
	{

		case 0:
		{
			if(R_chardata == 0x24)                 //报头标志
			{
				pUART->RecvBuff[0] = R_chardata;
				pUART->RecvParse.R_state = 1;
				pUART->RecvParse.checkSum = R_chardata;
			}
			else
			{
				uart1_error = -1;
			}
			break;
		}
		case 1:
		{
			if((R_chardata & 0xF0) == 0x60)         //部件编址：(视觉导航设备)06H，高四位发送方地址，低四位接收方地址。
			{
				pUART->RecvBuff[1] = R_chardata;
				pUART->RecvParse.R_state = 2;
				pUART->RecvParse.checkSum += R_chardata;
			}
			else
			{
				pUART->RecvParse.R_state = 0;
				uart1_error = -2;
			}
			break;
		}
		case 2:
		{
			pUART->RecvBuff[2] = R_chardata;           //功能码:0xA2(固定)
			pUART->RecvParse.R_state = 3;
			pUART->RecvParse.checkSum += R_chardata;
			break;
		}
		case 3:
		{
			pUART->RecvBuff[3] = R_chardata;          //数据字节长度:16(固定)
			pUART->RecvParse.R_state = 4;
			pUART->RecvParse.checkSum += R_chardata;
			pUART->RecvParse.p_recv_num = 4;
			switch(pUART->RecvBuff[2])                //根据功能码决定要接收不同的字节长度
			{
				case 0xA2:
				{
					pUART->RecvParse.length_recv_num = 7 + R_chardata;  //8-1+1*nData
					break;
				}
			
				default : break;
			}
			break;
		}
		case 4:
		{
			pUART->RecvBuff[pUART->RecvParse.p_recv_num++] = R_chardata;    //接收后面除校验和外的所有字节
			pUART->RecvParse.checkSum += R_chardata;

			if(pUART->RecvParse.p_recv_num >= pUART->RecvParse.length_recv_num)
				pUART->RecvParse.R_state = 5;
			break;
		}
		case 5:
		{
			pUART->RecvBuff[pUART->RecvParse.p_recv_num++] = R_chardata;    //校验和
			pUART->RecvParse.R_state = 0;
			if(pUART->RecvParse.checkSum == R_chardata)  //校验和通过
			{
				uart1_error = 1;
				ReceivedComPortDataEvent(USARTx, pUART->RecvBuff);
			}
			else
				uart1_error = -3;

			break;
		}
		default : pUART->RecvParse.R_state = 0; uart1_error = -4;break;

	}
}

//根据报文中的功能号确定报文信息属性
void ReceivedComPortDataEvent(USART_TypeDef* USARTx, uint8_t * buff)
{

	switch(buff[2])
	{

		case 0xA2:          //定时返回请求（定时返回数据）
		{
		
			GetDataFromVision(buff);
			break;
		}
		

		default : break;
	}
}

//更新视觉发送过来的图像偏差信息
void GetDataFromVision(uint8_t * buff)
{
	uint16_t startAddress = 0;
	uint8_t length_Byte = 0;
	uint8_t p_data_num = 6;   //数据段从报文的第 6 个字节开始

	startAddress = buff[4] | (buff[5] << 8);
	length_Byte = buff[3];
	while(length_Byte--)    //更新接收到的视觉的信息
	{
		visionflydata_usart.data_c[startAddress++] = buff[p_data_num++];
	}
	//成功收到数据后，发送消息给视觉系统
	//SendMessageToVision(USART2, 0x06, visionflydata_usart.data_c);  //发送数据给视觉系统
}

//更新视觉发送过来的信息（防止正在使用视觉数据的过程中，正好视觉发送新的数据过来，造成数据不一致）
void MdfReceiveMessage(void)
{
	//更新视觉数据
	tarPosX = visionflydata_usart.data_all.PosX_R;  //目标在X方向上的偏差，单位：mm
	tarPosY = visionflydata_usart.data_all.PosY_R;  //目标在Y方向上的偏差，单位：mm
	VisionLocking_flag = visionflydata_usart.data_all.State_R;  //视觉锁定目标的状态：0--目标丢失，1--正在锁定，2--已经锁定
			
}

//发送通信信息给视觉系统
void SendMessageToVision(USART_TypeDef* USARTx, uint8_t funCode, uint8_t * SendBuff)
{

    uint8_t checksum=0;
    uint8_t length_Byte = 38;
    uint16_t startAddress = 0x0010;
   // char * pdata = NULL;
    uint8_t byteArr[38];
    uint8_t i=0;
		uint8_t j=0;


    //帧头
    byteArr[i++] = 0x24;    //报头标志
    checksum += 0x24;

    byteArr[i++] = 0x06;            //部件编址：06H，高四位发送方地址，低四位接收方地址。
    checksum += 0x06;

    byteArr[i++] = funCode;                   //功能码
    checksum += funCode;

    byteArr[i++] = length_Byte;               //数据字节长度
    checksum += length_Byte;

    byteArr[i++] = startAddress & 0x00FF;    //数据起始地址,先低地址
    checksum += startAddress & 0x00FF;

    byteArr[i++] = startAddress >> 8;
    checksum += startAddress >> 8;

    //数据段
    while(length_Byte)                                //发送数据
    {
        byteArr[i++] = SendBuff[startAddress];
        checksum += SendBuff[startAddress];
        startAddress++;
        length_Byte--;
    }

    //帧尾
    byteArr[i++] = 0;                            //返回标志,0：不需要返回应答
    checksum += 0;
    byteArr[i++] = checksum;                //校验和
    while(i)    
		{
			i--;
			
			USART_SendData(USART2, byteArr[j++]);
			while(USART_GetFlagStatus(USART2,USART_FLAG_TXE) == RESET)
				;
		}
		i=0;
		
}

//更新发送给视觉系统的数据
void MdfSendMessageToVision(void)
{
	//更新发动给视觉的数据
	visionflydata_usart.data_all.TubID_S = 2;  //视觉执行2号任务，码放第一个桶
	visionflydata_usart.data_all.Differ_F_S = 2.0;  //飞机高度信息，单位：m		
}
