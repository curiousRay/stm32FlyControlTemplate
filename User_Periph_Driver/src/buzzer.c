#include "buzzer.h"

void Buzzer_Init(void)
{
	/*Configure Buffer*/
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

}
/*Test_Buzzer->检测蜂鸣器是否工作正常
void Test_Buzzer(void)
{

		Buzzer_ON;
		Delay(1000);
		Buzzer_OFF;
		Delay(1000);
}
*/


