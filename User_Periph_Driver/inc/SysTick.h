
#ifndef __SYSTICK_H
#define __SYSTICK_H
#include "stm32f4xx.h"

void SysTick_Init(void);
void TimingDelay_Decrement(void);
void Delay(__IO uint32_t nTime);

#endif
