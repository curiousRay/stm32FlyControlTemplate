/*-------------------------------------
*				ADC3多通道驱动程序
*	文件：	ADC3_DMA2.c ADC3_DMA2.h
*   作者：  孔大庆
*   描述：  ADC3 三路通道均可采集转换电压
*   说明:   参照主程序调用函数
			ADC3_DMA2_Config(ADC3ConvertedValue); ->"ADC3ConvertedValue"是定义变量ADC3ConvertedValue[3]的指针，注意参数是指针传递
            ADC_SoftwareStartConv(ADC3);->该函数来自固件库，必须紧随ADC3_DMA2_Config() 其后
			
	引脚：
		    ADC3_channel1 -> PA1
			ADC3_channel11 -> PC1
			ADC3_channel12 -> PC2
*   时间：  2015年7月5日
---------------------------------------*/

#ifndef __ADC3_DMA2_H
#define __ADC3_DMA2_H
#define ADC3_DR_ADDRESS     ((uint32_t)0x4001224C)
#include "stm32f4xx.h"
void ADC3_DMA2_Config(uint16_t *ADC_Value);

#endif
