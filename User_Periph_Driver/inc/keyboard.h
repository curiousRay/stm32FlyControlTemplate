/*-------------------------------------
*								4×4矩阵键盘驱动
*		文件：	keyboard.c keyboard.h
*   作者：  孔大庆
*   描述：  扫描4×4矩阵键盘并返回1-16编码值
*   说明:   包含两个函数
								void KeyBoard_Init(void)——初始化键盘输入输出口；
								u8 Read_KeyValue(void)——扫描键盘并返回编码值；
	引脚：
			PD0-PD3为输出
			PD4-PD7为输入
*   时间：  2014年12月1日
---------------------------------------*/

#ifndef __KEYBOARD_H
#define __KEYBOARD_H
#include "stm32f4xx.h"

void KeyBoard_Init(void);
u8 Read_KeyValue(void);

#endif

