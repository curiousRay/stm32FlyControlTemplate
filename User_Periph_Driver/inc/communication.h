/**
  ******************************************************************************
  * @file    communication.h
  * @author  zbl
  * @version V1.0.0
  * @date    17-7-2016
  * @brief   This file contains all the functions prototypes for the communication
	*          between UAV-vision and PTZ-controler.
  ******************************************************************************
  * @attention
  *
  * 
  *
  * <h2><center>&copy; COPYRIGHT 2016 zbl</center></h2>
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"



/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  communication Init Structure definition  
  */
	 
struct UART_TypeDefStruct
{
	uint8_t RecvBuff[256];  //接收缓冲器，最大接收256个字节
	struct UART_RecvParse
	{
		uint8_t R_state;       //接收的状态，0：接收报头，1:接收部件名称
		uint8_t p_recv_num;      //接收缓冲器的指针号
		uint8_t length_recv_num; //接收缓冲器应该接收的字节长度
		uint8_t checkSum;      //校验和
	}RecvParse;
};


union VisionFlyData_USART
{
	uint8_t data_c[54];    //uart1MessageInit()中也用到了54，需要同时修改
	struct Data_All
	{
		//视觉数据
		//Int8 Heading_S;  //方向角
		int16_t PosX_R;    //X轴位置，单位：m
		int16_t PosY_R;    //Y轴位置，单位：m

		uint8_t State_R;   //状态号
		uint8_t GetId_R;    //保留变量,共用体中内存按最长的对对齐，所以这里需要补上一个字节//找到的水桶号1-8
		uint8_t Keep1_R;   //图像中间数据
		uint8_t Keep2_R;   //图像中间数据

		uint8_t Keep3_R;   //图像中间数据
		uint8_t Keep4_R;   //图像中间数据
		int16_t Keep5_R;   //图像中间数据

		int16_t Keep6_R;   //图像中间数据
		int16_t Keep7_R;   //图像中间数据

		//飞控数据
		float UAVPosX_S; //飞行器X轴位置，单位：m
		float UAVPosY_S; //飞行器Y轴位置，单位：m
		float UAVPosZ_S; //飞行器Z轴位置，单位：m
		float UAVVelX_S; //飞行器X轴速度，单位：m/s
		float UAVVelY_S; //飞行器Y轴速度，单位：m/s
		float UAVVelZ_S; //飞行器Z轴速度，单位：m/s
		float Roll_S;    //滚转角Φ，单位：°
		float Pitch_S;   //俯仰角θ，单位：°
		//float Yaw_S;     //航向角Ψ，单位：°
		uint8_t TubID_S;   //任务桶号
		uint8_t Differ_F_S;    //GPS差分标志
		uint8_t VeidoCtrl_F_S; //视频测量参数进入闭环控制标志
		uint8_t AutoFly_STEP_S; //自动飞行模式下的进程

		uint8_t UAVPosXYValid_S;   //DGPS获得的XY坐标有效标志位
		uint8_t Range_flag_S;  //飞机进入目标1.5米范围标志位
	}data_all;
};
 
/* Exported constants --------------------------------------------------------*/
extern struct UART_TypeDefStruct pUART2;        //uart数据解包及接收缓冲相关结构体
extern union VisionFlyData_USART visionflydata_usart;   //视觉与飞控的通信联合�
/**
  * @}
  */

/**
  * @}
  */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
void UARTGetByte(USART_TypeDef* USARTx, uint8_t R_chardata, struct UART_TypeDefStruct * pUART);
void ReceivedComPortDataEvent(USART_TypeDef* USARTx, uint8_t * buff);
void GetDataFromVision(uint8_t * buff);
void MdfReceiveMessage(void);
void SendMessageToVision(USART_TypeDef* USARTx, uint8_t funCode, uint8_t * SendBuff);
void MdfSendMessageToVision(void);
#ifdef __cplusplus
}
#endif

#endif /* __COMMUNICATION_H */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2016/7/17 zbl *****END OF FILE****/
