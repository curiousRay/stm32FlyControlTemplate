/*-------------------------------------
*								LED驱动程序
*	文件：	buzzer.c buzzer.h
*   作者：  孔大庆
*   描述：  驱动discovery上四个LED亮灭
*   说明:   包含一个函数
			void Buzzer_Init(void);
						
	引脚：
			buzzer->PE8
*   时间：  2015年7月4日
---------------------------------------*/

#ifndef __BUZZER_H
#define __BUZZER_H
#include "stm32f4xx.h"
#define Buzzer_ON  GPIO_SetBits(GPIOE,GPIO_Pin_8)
#define Buzzer_OFF GPIO_ResetBits(GPIOE,GPIO_Pin_8)
void Buzzer_Init(void);
#endif
//


