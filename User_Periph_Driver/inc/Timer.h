#ifndef __TIMER_H
#define __TIMER_H
#include "stm32f4xx.h"

void TIM3_PWMDuty(uint16_t widthCH3, uint16_t widthCH4);
void TIM3_PWMOUT_Init(void);
void TIM2_Init(void);
void TIM2_ISR(void);
void TIM4_PWMIN_Init(void);
void TIM4_ISR(void);

#endif


