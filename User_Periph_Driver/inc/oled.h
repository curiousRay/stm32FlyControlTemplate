/**************引脚定义************************
	CS1->PE9  
    CS2->PE10
	CS3->PD11
	
	RES->PD10
	DC->PD9
	CLK(D0)->PB13
	DIN(D1)->PB15
*/

#ifndef __OLED_H
#define __OLED_H			  	 
#include "stm32f4xx.h"
#include "stdlib.h"	    	
//OLED模式设置
//0:4线串行模式
//1:并行8080模式
// SIZE 16或者12 16字体大于12
#define SIZE 16
#define XLevelL		0x02
#define XLevelH		0x10
#define Max_Column	128
#define Max_Row		64
#define	Brightness	0xFF 
#define X_WIDTH 	128
#define Y_WIDTH 	64	    						  
//-----------------OLED端口定义----------------  					   

#define OLED1_CS_Clr()  GPIO_ResetBits(GPIOE,GPIO_Pin_9)//CS
#define OLED1_CS_Set()  GPIO_SetBits(GPIOE,GPIO_Pin_9)
#define OLED2_CS_Clr()  GPIO_ResetBits(GPIOE,GPIO_Pin_10)//CS
#define OLED2_CS_Set()  GPIO_SetBits(GPIOE,GPIO_Pin_10)
#define OLED3_CS_Clr()  GPIO_ResetBits(GPIOD,GPIO_Pin_11)//CS
#define OLED3_CS_Set()  GPIO_SetBits(GPIOD,GPIO_Pin_11)

#define OLED_RST_Clr() GPIO_ResetBits(GPIOD,GPIO_Pin_10)//RES
#define OLED_RST_Set() GPIO_SetBits(GPIOD,GPIO_Pin_10)

#define OLED_DC_Clr() GPIO_ResetBits(GPIOD,GPIO_Pin_9)//DC
#define OLED_DC_Set() GPIO_SetBits(GPIOD,GPIO_Pin_9)




 		     
#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据
//OLED控制用函数
void OLED_WR_Byte(u8 dat,u8 cmd);	    
void OLED_Display_On(void);
void OLED_Display_Off(void);	   							   		    
void OLED_Init(void);
void OLED_Clear(void);
void OLED_DrawPoint(u8 x,u8 y,u8 t);
void OLED_Fill(u8 x1,u8 y1,u8 x2,u8 y2,u8 dot);
void OLED_ShowChar(u8 x,u8 y,u8 chr);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size);
void OLED_ShowString(u8 x,u8 y, u8 *p);	 
void OLED_Set_Pos(unsigned char x, unsigned char y);
void OLED_ShowCHinese(u8 x,u8 y,u8 no);
void OLED_DrawBMP(unsigned char x0, unsigned char y0,unsigned char x1, unsigned char y1,unsigned char BMP[]);
#endif  
	 



