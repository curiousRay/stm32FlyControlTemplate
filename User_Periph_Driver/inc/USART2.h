/*-------------------------------------
*					USART2驱动程序
*	文件：	usart2.c usart2.h
*   作者：  孔大庆
*   描述：  串口驱动模块
*   说明:   函数
            void USRAT2_Init(void);					
	引脚：
			USART2.Tx->PA2.0
			USART2.Rx->PA3.0
*   时间：  2014年12月1日
---------------------------------------*/
#ifndef __USART2_H
#define __USART2_H
#include "stm32f4xx.h"



void USART2_Init(void);
void USART2_ISR(void);
#endif


