/*-------------------------------------
*								LED驱动程序
*	文件：	led.c led.h
*   作者：  孔大庆
*   描述：  驱动discovery上四个LED亮灭
*   说明:   包含三个函数
							void LED_Init(void);
							void LED_ON(u8 led);
							void LED_OFF(u8 led);
	引脚：
			led1-> PD12
			led2-> PD13
			led3-> PD14
			led4-> PD15
*   时间：  2015年7月4日
---------------------------------------*/

#ifndef __LED_H
#define __LED_H
#include "stm32f4xx.h"

void LED_Init(void);
void LED_ON(uint8_t led);
void LED_OFF(uint8_t led);
#endif
/**
  * @}
  */

/**
  * @}
  */
