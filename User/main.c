#include "stm32f4_discovery.h"
#include "keyboard.h"
#include "LED.h"
//#include "oled.h"
//#include "SysTick.h"
//#include "buzzer.h"
#include "USART2.h"
#include "ADC3_DMA2.h"
//#include "TIMER.h"
//#include "communication.h"
#include <stdio.h>
//#include "ctrlaw.h"
//#include "ptztracing.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_syscfg.h"
#include "misc.h"

#ifdef __GNUC__
 /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printfset to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif

uint8_t OLED=1;
uint16_t nus=2000; //timer2时基1ms
uint16_t acount=0;
uint32_t IC2Value = 0;
uint32_t PWM_Input_Freq = 0; 
uint32_t m=0;
uint8_t time_100ms_flag=0;
uint16_t ADC3ConvertedVoltage[3];
uint16_t ADC3ConvertedValue[3];
int16_t tarPosX = 0; //目标在X方向上的偏差，单位：mm
int16_t tarPosY = 0; //目标在Y方向上的偏差，单位：mm
uint8_t VisionLocking_flag = 0;  //视觉锁定目标的状态：0--目标丢失，1--正在锁定，2--已经锁定
uint8_t b_FlagInitCtrlPTZVal = 1; //云台控制量初始化标志
	
	void delay_us(uint32_t time)  
{  
  uint32_t i=8*time;  
  while(i--);  
}  
	void delay_ms(uint32_t nTimer)  
{  
    uint32_t i=1000*nTimer;  
    delay_us(i);  
}  

	

int main(void)
{
	//按钮位于PA0
	
	//配置GPIO和SYSCFG时钟
  RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd (RCC_APB2Periph_SYSCFG, ENABLE);
	
	
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitSwitch;
	NVIC_InitTypeDef NVIC_InitSwitch;
	
	//配置引脚模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
	//连接中断线和GPIO
	
	//配置外部中断
	EXTI_InitSwitch.EXTI_Line = EXTI_Line0;
	EXTI_InitSwitch.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitSwitch.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitSwitch.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitSwitch);
	
	//配置优先级
	NVIC_InitSwitch.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitSwitch.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitSwitch.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitSwitch.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitSwitch);
	
  LED_Init();
		
	while(1)
  {
	LED_ON(1);
	delay_ms (1000);
	LED_ON(2);
	delay_ms (1000);
	LED_ON(3);
	delay_ms (1000);
	LED_ON(4);	
	delay_ms (1000);
	LED_OFF(1);
	delay_ms (1000);
	LED_OFF(2);
	delay_ms (1000);
	LED_OFF(3);
	delay_ms (1000);
	LED_OFF(4);	
	}
}

void EXTI0_IRQHandler(void)
{
	if (EXTI_GetITStatus(EXTI_Line0)!=RESET)
	{
	  {
			 LED_Init();
			LED_ON(1);
			LED_ON(2);
			LED_ON(3);
			LED_ON(4);
  	}
	
	EXTI_ClearITPendingBit(EXTI_Line0);
  }
}

